package com.example.kevin.todo_list

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference

class ListDetailRecyclerViewAdapter(ref : DatabaseReference):
        RecyclerView.Adapter<ListDetailRecyclerViewHolder>(){

    val todoTasks : ArrayList<TodoTask> = arrayListOf()

    init {
        ref.addChildEventListener(object : ChildEventListener{
            override fun onChildRemoved(item: DataSnapshot) {

                val deletedIndex = todoTasks.indexOfFirst { element -> element.id == item.key}
                todoTasks.removeAt(deletedIndex)
                notifyItemRemoved(deletedIndex)
            }

            override fun onChildAdded(item: DataSnapshot, p1: String?) {
                val taskTitle = item.child("taskName").value.toString()
                val taskId = item.key.toString()
               // val taskId = item.key.toString()

                //Log.d("TaskTitle",item.value.toString())
                //Log.d("TaskId",item.key.toString())

                todoTasks.add(TodoTask(taskId,taskTitle))


                notifyItemChanged(todoTasks.size)
            }

            override fun onChildMoved(item: DataSnapshot, p1: String?) {

            }

            override fun onChildChanged(item: DataSnapshot, p1: String?) {

            }

            override fun onCancelled(p0: DatabaseError) {

            }

        })

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListDetailRecyclerViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_selection_view_holder,parent,false)
        return ListDetailRecyclerViewHolder(view)
    }

    override fun getItemCount(): Int {
        return todoTasks.count()
    }

    override fun onBindViewHolder(holder: ListDetailRecyclerViewHolder, position: Int) {
        holder.taskTitle.text=todoTasks[position].taskName

       // holder.taskTitle.text = todoLists[position].listName
    }



}