package com.example.kevin.todo_list

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.widget.Button
import android.widget.EditText
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.util.*

class ListDetailActivity : AppCompatActivity(){

    val database = FirebaseDatabase.getInstance()
    val ref = database.getReference("todo-list")
    lateinit var tasksRecyclerView: RecyclerView
    internal lateinit var addToDoButton: Button
    internal lateinit var  nameOfList : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_detall)

        val listId = intent.getStringExtra(ActivityList.INTENT_LIST_ID)

        nameOfList = listId

        //Recicler view
        tasksRecyclerView = findViewById(R.id.recyclerView_listDetail)
        tasksRecyclerView.layoutManager = LinearLayoutManager(this)
        tasksRecyclerView.adapter = ListDetailRecyclerViewAdapter(ref.child(nameOfList).child("Pendientes"))



        addToDoButton = findViewById(R.id.button_addDetail)

        addToDoButton.setOnClickListener{
            _ -> showCreateDetail()
        }

        ref.child(listId).child("list-name")
                .addListenerForSingleValueEvent(object : ValueEventListener{
                    override fun onCancelled(p0: DatabaseError) {
                    }

                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        title = dataSnapshot.value.toString()
                    }
                })
    }

    private fun showCreateDetail(){
        val dialogTitle = getString(R.string.name_of_ToDo)
        val positiveButtonTitle = getString(R.string.create_ToDo)

        val builder = AlertDialog.Builder(this)
        val listTitleEditText = EditText(this)
        listTitleEditText.inputType = InputType.TYPE_CLASS_TEXT


        builder.setTitle(dialogTitle)
        builder.setView(listTitleEditText)

        builder.setPositiveButton(positiveButtonTitle){
            dialog, _ ->
            val newToDo = listTitleEditText.text.toString()

            val newId = "TASK"+UUID.randomUUID().toString()

            ref.child(nameOfList)
                    .child("Pendientes")
                    .child(newId)
                    .child("taskName")
                    .setValue(newToDo)
            dialog.dismiss()
        }

        builder.create().show()
    }



}